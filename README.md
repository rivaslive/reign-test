# Reign Test

Test created by Ing. Kevin Rivas.

## Installation

To install this project you only need to have Docker, Docker Compose, if you already have Docker Desktop it is not necessary to do any extra installation, it is also necessary to have installed a Linux subsystem this for Windows users, if you do not have Docker installed you can follow the steps installation on your computer at the following link:

[Get Started Docker](https://www.docker.com/get-started)

## Configure Project API Backend

### Database
By default a simple connection string is being used, in any case configure your own connection string in your custom MongoDB docker you can do it in the following file

api/src/app.module.ts

```javascript

import { TaskOneHourService } from './taskOneHour/taskOneHour.service';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    MongooseModule.forRoot('mongodb://<USERNAME>:<PASSWORD>@<HOST>:<PORT>/stories-db', {
      useNewUrlParser: true,
    }),
    StoryModule,
  ],
  controllers: [AppController],
  providers: [AppService, TaskOneHourService],
})
export class AppModule {}
```

Default ```mongodb://mongo/stories-db``` 
"mongo" refers to the MongoDB container image

## Configure Project Client FRONTEND

### HOST API
By default a network is being made to **http://localhost:5000/v1** which is the port on which the docker API will run, you can change it for a domain or for the server chain that the api is hosted on

created file
default: REACT_APP_API_HOST=http://localhost:5000/v1

client/.env.local
```
REACT_APP_API_HOST=<HOST_API>
```

## Docker Lauch

Now we just have to launch docker compose.

```bash
docker-compose build
docker-compose up
```

### Load to initial database

first make sure docker compose was able to build the container: kevin-reign-mongo database
```bash

docker container ps -a
```

Result:

```bash

CONTAINER ID   IMAGE               COMMAND                  CREATED       STATUS                     PORTS     NAMES
89d786359d22   reign-test_client   "docker-entrypoint.s…"   2 hours ago   Exited (137) 2 hours ago             kevin-reign-client
c0c7025d91fd   reign-test_api      "docker-entrypoint.s…"   2 hours ago   Exited (137) 2 hours ago             kevin-reign-api
1cea2569b725   mongo               "docker-entrypoint.s…"   2 hours ago   Exited (137) 2 hours ago             kevin-reign-mongo_database
```

if the container exists we enter the named kevin-reign-mongo database

```bash

docker exec -it kevin-reign-mongo_database bash
```
now we will load data to our DB

```bash
mongorestore --username <USERNAME> --password <PASSWORD> /backup/stories-db/*.bson
```

default:
```bash
mongorestore /backup/stories-db/*.bson
```

Access in your browser to:
[http://localhost:3000](http://localhost:3000)


## License
[MIT](https://choosealicense.com/licenses/mit/)



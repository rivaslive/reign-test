import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { StoryModule } from './story/story.module';
import { ScheduleModule } from '@nestjs/schedule';
import { TaskOneHourService } from './taskOneHour/taskOneHour.service';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    MongooseModule.forRoot('mongodb://mongo/stories-db', {
      useNewUrlParser: true,
    }),
    StoryModule,
  ],
  controllers: [AppController],
  providers: [AppService, TaskOneHourService],
})
export class AppModule {}

export class CreateStoryDTO {
  readonly objectID: string
  readonly title: string
  readonly story_title: string
  readonly url: string
  readonly story_url: string
  readonly author: string
  readonly created_at: string
}

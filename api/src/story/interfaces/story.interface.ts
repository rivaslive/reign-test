import {Document} from 'mongoose'
export interface Story extends Document{
  readonly objectID: string
  readonly title: string
  readonly story_title: string
  readonly url: string
  readonly story_url: string
  readonly author: string
  readonly created_at: string
  readonly status: 'actived' | 'lapsed' | 'banned'
}

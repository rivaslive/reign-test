import { Schema } from "mongoose";

export const StorySchema = new Schema({
  objectID: { type: String, required: true },
  title: String,
  story_title: String,
  url: String,
  story_url: String,
  author: { type: String, required: true },
  created_at: Date,
  status:{ type: String, enum: ['actived', 'lapsed', 'banned'], default: 'actived' },
})

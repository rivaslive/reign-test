import * as request from 'supertest';
import { AppModule } from '../app.module';
import { Test, TestingModule } from '@nestjs/testing';

describe('Stories', () => {
  let app;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });
  it(`/GET stories`, () => {
    return request(app.getHttpServer()).get('/stories').expect(200);
  });
});

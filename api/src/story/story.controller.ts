import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  NotFoundException,
  Param,
  Post,
  Res,
} from '@nestjs/common';
import { StoryService } from './story.service';
import { CreateStoryDTO } from './dto/story.dto';

@Controller('stories')
export class StoryController {
  constructor(private StoryService: StoryService) {}

  @Get('/')
  async getStories(@Res() res) {
    const stories = await this.StoryService.getStories();
    return res.status(HttpStatus.OK).json({ stories });
  }

  @Post('/')
  async createStory(@Res() res, @Body() createStoryDTO: CreateStoryDTO) {
    const story = await this.StoryService.setStory(createStoryDTO);
    return res.status(HttpStatus.OK).json(story);
  }

  @Delete('/:storyID')
  async removeStory(@Res() res, @Param('storyID') storyId) {
    if (!storyId)
      throw new BadRequestException(
        'Please, specify a storyId for remove from list',
      );
    const story = await this.StoryService.deleteStory(storyId);
    if (!story) throw new NotFoundException('Story not exists');
    return res.status(HttpStatus.OK).json(story);
  }
}

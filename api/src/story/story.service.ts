import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateStoryDTO } from './dto/story.dto';
import { Story } from './interfaces/story.interface';

@Injectable()
export class StoryService {
  constructor(
    @InjectModel('Story') private readonly StoryModel: Model<Story>,
  ) {}

  async getStories(): Promise<Story[]> {
    return this.StoryModel.find({ status: 'actived' }).sort({
      created_at: -1,
    });
  }

  async setStory(createStoryDTO: CreateStoryDTO): Promise<Story> {
    if (!createStoryDTO.story_title && createStoryDTO.title) return null;
    const find: any = await this.StoryModel.find({
      objectID: createStoryDTO.objectID,
    });
    if (!!find.length) return null;
    try {
      const story = new this.StoryModel(createStoryDTO);
      return story.save();
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  async deleteStory(storyId: string): Promise<Story | null> {
    try {
      return await this.StoryModel.findOneAndUpdate(
        { objectID: storyId },
        { status: 'banned' },
      );
    } catch (e) {
      console.log(e);
      return e;
    }
  }
}

import axios from 'axios';
import { Cron  } from '@nestjs/schedule';
import { Injectable, Logger } from '@nestjs/common';
import {StoryService} from '../story/story.service';
import { Story } from "../story/interfaces/story.interface";

@Injectable()
export class TaskOneHourService {
  constructor(private StoryService: StoryService) {}
  private readonly logger = new Logger(TaskOneHourService.name);
  
  @Cron('59 * * * *')
  async handleCron() {
    const {hits}: IHNResult  = await getFetchHits()
    hits.map(async (h) => {
      await this.StoryService.setStory(h)
    })
  }
}

const getFetchHits = async () => {
  return axios.get<IHNResult>('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
  .then(({data}) => (data))
    .catch(e => ({hits: []}))
}

interface IHNResult {
   hits: Story[]
}

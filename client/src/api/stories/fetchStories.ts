import axios from 'axios';
import {IHits} from "interfaces/api/hits";
import {useState, useEffect, useCallback} from 'react';

interface IResHits {
  stories: IHits[]
}

export const useFetchStories = () => {
  const [data, setData] = useState<IResHits>({stories: []})
  const [state, setState] = useState({loading: true, error: null})
  
  const getData = useCallback(() => {
      axios.get<IResHits>(`${process.env.REACT_APP_API_HOST}/stories`)
      .then(({data}) => {
        setData(data)
        setState({loading: false, error: null})
      }).catch((err) => {
        setState({loading: false, error: err?.message || err?.response?.data})
      })
    }, []);
  
  
  useEffect(() => {
    getData()
  }, [getData])
  
  return {data, ...state, getData}
}


export const removeStory = (objectID: string) => {
  return axios.delete<any>(`${process.env.REACT_APP_API_HOST}/stories/${objectID}`)
    .then(res => ({success: true}))
    .catch(err => ({success: false}))
}

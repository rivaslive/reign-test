import React from 'react';
import styles from './style.module.css'

const Header = () => {
  return <div className={styles.header}>
      <h1 className='text-white'>HN Feed</h1>
    <h3 className='text-white'>We {`<3`} hacker news!</h3>
  </div>
}

export default Header

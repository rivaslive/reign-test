import moment from 'moment'
import React from 'react';
import styles from './style.module.css'
import {IHits} from "interfaces/api/hits";
import {formatDateList} from "utils/utils";

interface IAction {
  onDelete: (objectID: string) => void
}

interface IList extends IHits, IAction {}

const ListElement = (props: IList) => {
  const {author, created_at, objectID, story_title, story_url, title, url, onDelete} = props;
  
  const handleDelete = (e: any) => {
    e.stopPropagation();
    return onDelete(objectID)
  }
  
  const onClick = () => {
    const href = story_url || url
    if (!!href) window.open(href, '_blank');
  }
  
  return <div className={styles.listElement} onClick={onClick}>
    <div className={styles.title}>
      {story_title || title}
      <span className={styles.subTitle}>- {author} -</span>
    </div>
    <div className={styles.action}>
      {
        moment(created_at).calendar(null, formatDateList)}
      <div className={styles.trash} title='Delete Story' onClick={handleDelete}>
        <img src="https://img.icons8.com/metro/16/E82900/delete.png" alt='icon-trash'/>
      </div>
    </div>
  </div>
}

export default ListElement;

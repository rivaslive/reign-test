import React from 'react';
import {HashLoader} from 'react-spinners'

const Loading = (props: IProps) => {
  return <div style={{padding: '40px', textAlign: 'center'}}>
    <HashLoader />
  </div>
}

interface IProps {

}

export default Loading;

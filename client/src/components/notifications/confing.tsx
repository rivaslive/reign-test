import Notification from "rc-notification";
import {CheckSVG} from "../check";
import React from "react";

let Message: any = null;
Notification.newInstance(
  {
    maxCount: 5,
    style: {'top': 10, left: '50%', transform: 'translateX(-50%)'}
  },
  n => {
    Message = n;
  },
);

export const Content = () => (
  <span className='d-flex' style={{fontSize: '14px', color: '#333'}}>
    <div style={{color: '#52c41a', paddingTop: 2, paddingRight: 5}}>
      <CheckSVG/>
    </div>
    Deleted!
  </span>
);

export default Message

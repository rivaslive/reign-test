import React from "react";
import Message, {Content} from "./confing";

export const SuccessDeleteMessage = () => {
  Message.notice({
    duration: 5,
    style: {
      padding: '5px 16px 3px',
      background: '#fff',
      borderRadius: '2px',
      boxShadow: '0 3px 6px -4px rgb(0 0 0 / 12%), 0 6px 16px 0 rgb(0 0 0 / 8%), 0 9px 28px 8px rgb(0 0 0 / 5%)',
      pointerEvents: 'all'
    },
    content: <Content/>,
  });
}

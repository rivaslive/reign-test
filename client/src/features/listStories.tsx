import React from 'react';
import styles from './style.module.css'
import Loading from "components/loading/loading";
import ListElement from "components/list/listElement";
import {SuccessDeleteMessage} from "components/notifications";
import {removeStory, useFetchStories} from "api/stories/fetchStories";


const ListStories = () => {
  const {data, loading, getData} = useFetchStories();
  
  const deleteAction = async (objectID: string) => {
    const {success} = await removeStory(objectID)
    if (success) {
      getData()
      SuccessDeleteMessage()
    }
  }
  
  return <section>
    
    <div className={styles.listWrapper}>
      {
        data.stories.map(story => <ListElement key={story.objectID} {...story} onDelete={deleteAction}/>)
      }
      {
        loading && <Loading/>
      }
    </div>
  </section>
}

export default ListStories;

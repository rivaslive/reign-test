export interface IHits {
  objectID: string
  title?: string
  story_title?: string
  url?: string
  story_url?: string
  author: string
  created_at: string
  status: 'actived' | 'lapsed' | 'banned'
}

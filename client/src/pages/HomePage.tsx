import React from 'react';
import Header from "components/header/header";
import ListStories from "features/listStories";

const HomePage = (props: IProps) => {
  return <div>
    <Header/>
    <main className='container'>
      <ListStories/>
    </main>
  </div>
}

interface IProps {

}

export default HomePage;

export const formatDateList = {
  lastDay: '[Yesterday]',
  sameDay: '[] hh:mm a',
  nextDay: '[Tomorrow]',
  lastWeek: 'dddd',
  nextWeek: 'dddd',
  sameElse: 'L'
}
